import React, { Component } from 'react';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import close from '../../resources/images/close.svg';
// import cienc from '../../resources/images/ciens.svg';
import cast from '../../cast.json';

class Header extends Component {
  state = {
    headerShow: false
  };

  componentDidMount() {
    window.addEventListener('scroll', this.handleScroll);
  }

  render() {
    let x = [{ ...cast }];
    // console.log(x[0].Year)
    return (
      <div>
        <AppBar
          position="fixed"
          style={{
            backgroundColor: this.state.headerShow ? '#000000' : 'transparent',
            boxShadow: 'none',
            padding: '20px 0px'
          }}>
          <Toolbar>
            <div className="header_logo">
              <div className="font_nunito header_logo_penny">{x[0].Title}</div>
              <div className="header_logo_title">
                <ul className="breadcrumb">
                  <li>80 % INDICADO</li>
                  <li>SCIENCE FICTION</li>
                  <li>{x[0].Year}</li>
                  <li>USA</li>
                  <li>14</li>
                </ul>
              </div>
            </div>
            <IconButton
              aria-label="Menu"
              color="inherit"
              onClick={() => console.log('close button clicked')}>
              <img src={close} className="close_logo" alt="close" />
            </IconButton>
          </Toolbar>
        </AppBar>
      </div>
    );
  }
}
export default Header;
