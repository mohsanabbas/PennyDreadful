import React from 'react';
import PropTypes from 'prop-types';
// import { withStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Typography from '@material-ui/core/Typography';
import add from '../../resources/images/add.svg';
import sad from '../../resources/images/sad.svg';
import rec from '../../resources/images/rec.svg';
import share from '../../resources/images/share.svg';
import telecine from '../../resources/images/telecine.svg'
import show from '../../show.json';
import cast from '../../cast.json';

function TabContainer(props) {
  return (
    <Typography component="div" style={{ padding: 8 * 5}}>
      {props.children}
    </Typography>
  );
}

TabContainer.propTypes = {
  children: PropTypes.node.isRequired
};

// const styles = theme => ({
//   root: {
//     flexGrow: 1,
//     backgroundColor: '#1f262a'
//   }
// });

class Footer extends React.Component {
  state = {
    value: 0
  };

  handleChange = (event, value) => {
    this.setState({ value });
  };

  render() {
    // const { classes } = this.props;
    const { value } = this.state;
    let x = show.find(element => {
      return element;
    });
    // console.log(x.Synopsis);
    let y = [{ ...cast }];

    let p = y.slice(0);
    let names = [...p[0].Cast];
    let arrOfNames = names.map((el, idx) => {
      return `${names[idx].Name}`;
    });

  
    // console.log(actorsName)

    return (
      <div style={{
        flexGrow: 1,
   backgroundColor: '#1f262a'
      }}>
        <AppBar
          position="static"
          style={{
            backgroundColor: '#1f262a'
          }}>
          <Tabs
            value={value}
            onChange={this.handleChange}
            style={{
              marginLeft:'50px',
              marginRight:'50px',
              borderBottom:'1px solid grey'
            }}
            >
            <Tab label="General" />
            <Tab label="Elenco" />
            <Tab label="Principales premios" />
            <img src={telecine} className="telecine" alt="telecine_logo" />

          </Tabs>
        </AppBar>
        {value === 0 && (
          <TabContainer >
            <div className="footer_container" >
              <div className="d-flex flex-row">
                <div className="p-2">
                  <div>
                    <img src={add} className="cl_icon" alt="add_list" />
                    <p className="tcolor">My List</p>
                  </div>
                </div>
                <div className="p-2">
                  <div>
                    <img src={sad} className="cl_icon" alt="evaluar" />
                    <p className="tcolor">Evaluar</p>
                  </div>
                </div>
                <div className="p-2">
                  <div>
                    <img src={rec} className="cl_icon" alt="grabar" />
                    <p className="tcolor">Grabar</p>
                  </div>
                </div>
                <div className="p-2">
                  <div>
                    <img src={share} className="cl_icon" alt="share" />
                    <p className="tcolor">Compartir</p>
                  </div>
                </div>
              </div>
              <div style={{ margin: 0 *8}}>
                <h4>SYNOPSIS</h4>
                <p>{x.Synopsis}</p>
              </div>
            </div>
          </TabContainer>
        )}
        {value === 1 && (
          <TabContainer>
            <div>
              <ul className="flex-container">
                <li className="flex-item">{arrOfNames[0]}</li>
                <li className="flex-item">{arrOfNames[1]}</li>
                <li className="flex-item">{arrOfNames[2]}</li>
                <li className="flex-item">{arrOfNames[0]}</li>
                <li className="flex-item">{arrOfNames[1]}</li>
              </ul>
            </div>
          </TabContainer>
        )}
        {value === 2 && <TabContainer></TabContainer>}
      </div>
    );
  }
}

export default Footer;
