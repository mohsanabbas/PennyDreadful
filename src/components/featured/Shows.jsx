import React from 'react';
import { Accordion } from 'semantic-ui-react';
import { Tabs, Tab, TabPanel, TabList } from 'react-web-tabs';
import 'react-web-tabs/dist/react-web-tabs.css';
import play from '../../resources/images/play.svg';

class Shows extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      activeIndex: 0,
      shows: [],
      isLoaded: false
    };
  }
  handleClick = (e, titleProps) => {
    const { index } = titleProps;
    const { activeIndex } = this.state;
    const newIndex = activeIndex === index ? -1 : index;

    this.setState({ activeIndex: newIndex });
  };

  componentDidMount() {
    fetch('https://sample-api-78c77.firebaseio.com/episodes/SHOW123.json')
      .then(res => res.json())
      .then(data => {
        this.setState({
          isLoaded: true,
          shows: data
        });
      });
  }

  render() {
    const { isLoaded, shows, activeIndex } = this.state;

    if (!isLoaded) {
      return <div> Loading...</div>;
    } else {
      return (
        // <div style={{position:'relative'}}>
        <div className="shows_block">
          <div className="wrapper">
            <Tabs
              defaultTab="one"
              onChange={tabId => {
                console.log(tabId);
              }}>
              <TabList>
                <Tab tabFor="one" style={{ color: '#ffffff' }}>
                  T1
                </Tab>
                <Tab tabFor="two" style={{ color: '#ffffff' }}>
                  T2
                </Tab>
                <Tab tabFor="three" style={{ color: '#ffffff' }}>
                  T3
                </Tab>
              </TabList>
              <TabPanel tabId="one">

                <Accordion>
                  <Accordion.Title
                    active={activeIndex === 0}
                    index={0}
                    onClick={this.handleClick}
                    style={{
                      fontSize: '15px',
                      color: '#fff',
                      borderBottom: '1px solid #434547',
                      fontFamily: 'Nunito'
                    }}>
                    {shows[0].Title}
                    <img
                      src={play}
                      alt="play"
                      style={{
                        width: '20px',
                        marginLeft: '155px'
                      }}
                    />
                  </Accordion.Title>

                  <Accordion.Content active={activeIndex === 0}>
                    <div
                      style={{
                        width: '350px'
                      }}>
                      <img
                        style={{
                          width: '300px',
                          height: '150px',
                          padding: '4px'
                        }}
                        src={shows[0].Image}
                        alt=""
                      />
                      <hr />
                      <p
                        style={{
                          fontSize: '14px',
                          width: '350px',
                          color: '#fff',
                          paddingBottom:'10px',
                        }}>
                        {shows[0].Synopsis}
                      </p>
                    </div>
                  </Accordion.Content>
                </Accordion>
                {/* {shows.map((item,idx) =>
                          item !== null ?  (
                            <ul class="custom-list">
                            <li key={item.ID} >
                              {item.SeasonNumber === 1 ? `${item.Title}`:item.Title }
                            </li>
                            </ul>
                          )
                          :null
                        )} */}
                <Accordion>
                  <Accordion.Title
                    active={activeIndex === 1}
                    index={1}
                    onClick={this.handleClick}
                    style={{
                      fontSize: '15px',
                      color: '#fff',
                      borderBottom: '1px solid #434547',
                      fontFamily: 'Nunito'
                    }}>
                    {shows[1].Title}
                    <img
                      src={play}
                      alt="play"
                      style={{
                        width: '20px',
                        marginLeft: '170px'
                      }}
                    />
                  </Accordion.Title>

                  <Accordion.Content active={activeIndex === 1}>
                    <div
                      style={{
                        width: '350px'
                      }}>
                      <img
                        style={{
                          width: '300px',
                          height: '150px',
                          padding: '4px'
                        }}
                        src={shows[1].Image}
                        alt=""
                      />
                      <hr />
                      <p
                        style={{
                          fontSize: '14px',
                          width: '350px',
                          paddingBottom:'10px',
                          color: '#fff'
                        }}>
                        {shows[1].Synopsis}
                      </p>
                    </div>
                  </Accordion.Content>
                </Accordion>

                <Accordion>
                  <Accordion.Title
                    active={activeIndex === 2}
                    index={2}
                    onClick={this.handleClick}
                    style={{
                      fontSize: '15px',
                      color: '#fff',
                      borderBottom: '1px solid #434547',
                      fontFamily: 'Nunito'
                    }}>
                    {shows[3].Title}
                    <img
                      src={play}
                      alt="play"
                      style={{
                        width: '20px',
                        marginLeft: '186px'
                      }}
                    />
                  </Accordion.Title>

                  <Accordion.Content active={activeIndex === 2}>
                    <div
                      style={{
                        width: '350px'
                      }}>
                      <img
                        style={{
                          width: '300px',
                          height: '150px',
                          padding: '4px'
                        }}
                        src={shows[3].Image}
                        alt=""
                      />
                      <hr />
                      <p
                        style={{
                          fontSize: '14px',
                          width: '350px',
                          color: '#fff',
                          paddingBottom:'10px',
                        }}>
                        {shows[3].Synopsis}
                      </p>
                    </div>
                  </Accordion.Content>
                </Accordion>

                <Accordion>
                  <Accordion.Title
                    active={activeIndex === 3}
                    index={3}
                    onClick={this.handleClick}
                    style={{
                      fontSize: '15px',
                      color: '#fff',
                      borderBottom: '1px solid #434547',
                      fontFamily: 'Nunito'
                    }}>
                    {shows[4].Title}
                    <img
                      src={play}
                      alt="play"
                      style={{
                        width: '20px',
                        marginLeft: '200px'
                      }}
                    />
                  </Accordion.Title>

                  <Accordion.Content active={activeIndex === 3}>
                    <div
                      style={{
                        width: '350px'
                      }}>
                      <img
                        style={{
                          width: '300px',
                          height: '150px',
                          padding: '4px'
                        }}
                        src={shows[4].Image}
                        alt=""
                      />
                      <hr />
                      <p
                        style={{
                          fontSize: '14px',
                          width: '350px',
                          color: '#fff',
                          paddingBottom:'10px',
                        }}>
                        {shows[4].Synopsis}
                      </p>
                    </div>
                  </Accordion.Content>
                </Accordion>
              </TabPanel>

{/* Tab 2 */}


              <TabPanel tabId="two">
              <Accordion>
                  <Accordion.Title
                    active={activeIndex === 4}
                    index={4}
                    onClick={this.handleClick}
                    style={{
                      fontSize: '15px',
                      color: '#fff',
                      borderBottom: '1px solid #434547',
                      fontFamily: 'Nunito'
                    }}>
                    {shows[5].Title}
                    <img
                      src={play}
                      alt="play"
                      style={{
                        width: '20px',
                        marginLeft: '175px'
                      }}
                    />
                  </Accordion.Title>

                  <Accordion.Content active={activeIndex === 4}>
                    <div
                      style={{
                        width: '350px'
                      }}>
                      <img
                        style={{
                          width: '300px',
                          height: '150px',
                          padding: '10px'
                        }}
                        src={shows[5].Image}
                        alt=""
                      />
                      <hr />
                      <p
                        style={{
                          fontSize: '14px',
                          width: '350px',
                          color: '#fff',
                          paddingBottom:'10px',
                        }}>
                        {shows[5].Synopsis}
                      </p>
                    </div>
                  </Accordion.Content>
                </Accordion>
                
                <Accordion>
                  <Accordion.Title
                    active={activeIndex === 5}
                    index={5}
                    onClick={this.handleClick}
                    style={{
                      fontSize: '15px',
                      color: '#fff',
                      borderBottom: '1px solid #434547',
                      fontFamily: 'Nunito'
                    }}>
                    {shows[6].Title}
                    <img
                      src={play}
                      alt="play"
                      style={{
                        width: '20px',
                        marginLeft: '202px'
                      }}
                    />
                  </Accordion.Title>

                  <Accordion.Content active={activeIndex === 5}>
                    <div
                      style={{
                        width: '350px'
                      }}>
                      <img
                        style={{
                          width: '300px',
                          height: '150px',
                          padding:'10px',
                        }}
                        src={shows[6].Image}
                        alt=""
                      />
                      <hr />
                      <p
                        style={{
                          fontSize: '14px',
                          width: '350px',
                          color: '#fff',
                          paddingBottom:'10px',
                        }}>
                        {shows[6].Synopsis}
                      </p>
                    </div>
                  </Accordion.Content>
                </Accordion>

                <Accordion>
                  <Accordion.Title
                    active={activeIndex === 6}
                    index={6}
                    onClick={this.handleClick}
                    style={{
                      fontSize: '15px',
                      color: '#fff',
                      borderBottom: '1px solid #434547',
                      fontFamily: 'Nunito'
                    }}>
                    {shows[7].Title}
                    <img
                      src={play}
                      alt="play"
                      style={{
                        width: '20px',
                        marginLeft: '208px'
                      }}
                    />
                  </Accordion.Title>

                  <Accordion.Content active={activeIndex === 6}>
                    <div
                      style={{
                        width: '350px'
                      }}>
                      <img
                        style={{
                          width: '300px',
                          height: '150px',
                          padding:'10px',
                        }}
                        src={shows[7].Image}
                        alt=""
                      />
                      <hr />
                      <p
                        style={{
                          fontSize: '14px',
                          width: '350px',
                          color: '#fff',
                          paddingBottom:'10px',
                        }}>
                        {shows[7].Synopsis}
                      </p>
                    </div>
                  </Accordion.Content>
                </Accordion>

                <Accordion>
                  <Accordion.Title
                    active={activeIndex === 7}
                    index={7}
                    onClick={this.handleClick}
                    style={{
                      fontSize: '15px',
                      color: '#fff',
                      borderBottom: '1px solid #434547',
                      fontFamily: 'Nunito'
                    }}>
                    {shows[8].Title}
                    <img
                      src={play}
                      alt="play"
                      style={{
                        width: '20px',
                        marginLeft: '20px'
                      }}
                    />
                  </Accordion.Title>

                  <Accordion.Content active={activeIndex === 7}>
                    <div
                      style={{
                        width: '350px'
                      }}>
                      <img
                        style={{
                          width: '300px',
                          height: '150px',
                          padding:'10px',

                        }}
                        src={shows[8].Image}
                        alt=""
                      />
                      <hr />
                      <p
                        style={{
                          fontSize: '14px',
                          width: '350px',
                          color: '#fff',
                          paddingBottom:'10px',
                        }}>
                        {shows[8].Synopsis}
                      </p>
                    </div>
                  </Accordion.Content>
                </Accordion>
              </TabPanel>
                

              
              <TabPanel tabId="three">
                {shows.map((item, idx) =>
                  item === null ? null : (
                    <ul class="custom-list">
                      <li key={item.ID}>
                        {item.SeasonNumber === 3
                          ? `${item.Title}`
                          : 'Content not found'}
                      </li>
                    </ul>
                  )
                )}
              </TabPanel>
            </Tabs>
          </div>
        </div>
      );
    }
  }
}
export default Shows;
