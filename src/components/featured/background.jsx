import React from 'react';
import Slider from 'react-slick';
import cast from '../../cast.json';
import pennydreadful from '../../resources/images/pennydreadful.jpg';

const Background = () => {
  // let x = [{ ...cast }];
  //   console.log(x[0].Images.Background);

  const settings = {
    dots: false,
    infinite: false,
    autoplay: false,
    slidesToScroll: 1,
    speed: 1000
  };

  return (
    <div
      className="carrousel_wrapper"
      style={{
        //   background:'red',
        // background: `url(${x[0].Images.Background})`
        height: `${window.innerHeight}px`,
        overflow: 'hidden'
      }}>
      <Slider {...settings}>
        <div>
          <div
            className="carrousel_image"
            style={{
              // background: ` url(${x[0].Images.Background})`,
              background: `url(${pennydreadful})`,

              height: `${window.innerHeight}px`,
              backgroundSize: 'cover',
              zIndex:-5,
              backgroundColor:'rgba(0,0,0,0.1)',

            }}
          />
        </div>
      </Slider>
    </div>
  );
};
export default Background;
