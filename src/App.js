import React, { Component } from 'react';
// import './resources/styles.css';
import css from './resources/scss/style.css';

import Header from './components/header_footer/Header';
import Featured from './components/featured/index';
import Footer from './components/header_footer/Footer';


class App extends Component {
  
  
 

  
  render() {

    return (
      
     <div className="App">
        <Header />
        <Featured />
        
        
        <Footer />

      
      </div>
      
      
    );
    
  }
}

export default App;
